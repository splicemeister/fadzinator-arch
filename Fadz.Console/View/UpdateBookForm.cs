﻿using Fadz.Entity;
using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using console = System.Console;

namespace Fadz.Console
{
    public class UpdateBookForm
    {
        ILibraryService svc = null;
        public UpdateBookForm(ILibraryService service)
        {
            svc = service;
        }

        public void Run()
        {
            console.WriteLine("-----------------------------");
            console.WriteLine("Update Book");


            console.Write("Enter Id of the Book to be Updated: ");
            string strId = console.ReadLine();
            int id = 0;
            int count = 0;

            while (!int.TryParse(strId, out id))
            {
                console.Write("Please entery as valid Id (Integer) :");
                strId = console.ReadLine();
                count++;
                if (count % 5 == 0) //if you try to flood the screen, i will clean it for you
                {
                    console.Clear();
                    console.WriteLine("-----------------------------");
                    console.WriteLine("Update Book");

                }
            }
            try
            {
                var book = svc.GetBookById(id);
                if (book != null)
                {
                    string newTitle = "";
                    console.WriteLine("Id: {0}",book.Id);
                    console.WriteLine("Title: {0}", book.Title);
                    console.Write("Enter New Title: ");
                    newTitle = console.ReadLine();

                    book.Title = newTitle;
                    svc.UpdateBook(book);
                    console.WriteLine("Update success.");
                }
                else
                {
                    console.WriteLine("Book Id does not exist");
                }
            }
            catch (Exception)
            {
                console.WriteLine("Ooops something wrong happen. The monkey is eating pineapple.");
            }
            console.WriteLine("-----------------------------");
        }
    }
}
