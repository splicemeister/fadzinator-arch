﻿using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using console = System.Console;

namespace Fadz.Console
{
    public class ListBooksForm
    {
        ILibraryService svc = null;
        public ListBooksForm(ILibraryService service)
        {
            svc = service;
        }

        public void Run()
        {
            console.WriteLine("-----------------------------");
            console.WriteLine("List of Books");
            var books = svc.GetAllBooks(true);
            foreach (var book in books)
            {
                console.WriteLine("Id: {0}\t title: {1}", book.Id, book.Title);
            }
            console.WriteLine("-----------------------------");
        }
    }
}
