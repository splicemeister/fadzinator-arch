﻿using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using console = System.Console;

namespace Fadz.Console
{
    //In some way it act also same way as your controller in asp.net
    public class ViewManager
    {

        ContainerFactory con;
        string action = "r";

        public ViewManager()
        {
            con = ContainerFactory.Instance;
            action = ViewBooks();
            do
            {
                switch (action)
                {
                    case "a":
                    case "A":
                        action = AddBook();
                        break;
                    case "u":
                    case "U":
                        action = UpdateBook();
                        break;
                    case "r":
                    case "R":
                    default:
                        action = ViewBooks();
                        break;

                }
            }
            while (action != "x" && action != "X");

        }

        public string ViewBooks()
        {
            console.Clear();
            DisplayMenus();
            var viewList = con.Container.GetInstance<ListBooksForm>();
            viewList.Run();
            return GetAction();
        }

        public string AddBook()
        {
            console.Clear();
            DisplayMenus();
            var addBook = con.Container.GetInstance<AddBookForm>();
            addBook.Run();
            return GetAction();
        }

        public string UpdateBook()
        {
            console.Clear();
            DisplayMenus();
            var addBook = con.Container.GetInstance<UpdateBookForm>();
            addBook.Run();
            return GetAction();
        }

        private void DisplayMenus()
        {
            console.WriteLine("\na=add, u=update, d=delete, r=refresh, x=exit \n");
        }

        private string GetAction()
        {
            console.Write("Enter your desired action: ");
            return console.ReadLine();
        }
    }
}
