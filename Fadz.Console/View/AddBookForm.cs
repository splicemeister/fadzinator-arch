﻿using Fadz.Entity;
using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using console = System.Console;

namespace Fadz.Console
{
    public class AddBookForm
    {
        ILibraryService svc = null;
        public AddBookForm(ILibraryService service)
        {
            svc = service;
        }

        public void Run()
        {
            console.WriteLine("-----------------------------");
            console.WriteLine("Add Book");


            console.Write("Enter Title: ");
            string title = console.ReadLine();
            var book = new Book()
            {
                Title = title
            };

            try
            {
                svc.AddBook(book);
                console.WriteLine("\n{0} is successfully added. \nplease refresh the form", title);
            }
            catch (Exception)
            {
                console.WriteLine("Ooops something wrong happen. The monkey is eating pineapple.");
            }
            console.WriteLine("-----------------------------");
        }
    }
}
