﻿using Fadz.Data;
using Fadz.Interface;
using Fadz.Service;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fadz.Console
{
    /// <summary>
    /// Container Factory follows the singleton/factory pattern.
    /// So that only once instance of the container will be created.
    /// </summary>
    public class ContainerFactory
    {
        private static ContainerFactory instance;
        public static ContainerFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ContainerFactory();
                    return instance;
                }
                return instance;
            }
        }

        private ServiceContainer container;
        public ServiceContainer Container { get { return container; } }

        /// <summary>
        /// Private default Constructors
        /// Also will serve as the registration entry for your objects.
        /// </summary>
        private ContainerFactory()
        {
            container = new ServiceContainer();
            container.Register<FadzDbContext>();
            container.Register<IDataService, DataService>();
            container.Register<ILibraryService, LibraryService>();

            //Views
            container.Register<ViewManager>(); 
            container.Register<ListBooksForm>();
            container.Register<AddBookForm>();
            container.Register<UpdateBookForm>(); 
            
            
        }
    }
}
