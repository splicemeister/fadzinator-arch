﻿using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fadz.Data;
using Fadz.Entity;

namespace Fadz.Service
{
    public class DataService : IDataService
    {
        private FadzDbContext context;

        private IRepository<Book> book = null;
        private IRepository<Category> category = null;

        public IRepository<Book> Book { get { return book; } }
        public IRepository<Category> Category { get { return category; } }

        public DataService(FadzDbContext fadzContext)
        {
            context = fadzContext;
            book = new FadzRepository<Book>(context);
            category = new FadzRepository<Category>(context);
        }
    }
}
