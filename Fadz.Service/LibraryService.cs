﻿using Fadz.Entity;
using Fadz.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fadz.Service
{
    public class LibraryService : BaseService<LibraryService>, ILibraryService
    {
        private IDataService dataSvc;

        public LibraryService(IDataService svc)
        {
            dataSvc = svc;
        }

        public int AddBook(Book book)
        {
            int id = 0;
            try
            {
                dataSvc.Book.Add(book);
                dataSvc.Book.Commit();
                id = book.Id;
            }
            catch (Exception e)
            {
                //TODO: Logging
                throw e;
            }
            return id;
        }

        public Book GetBookById(int id)
        {
            if (id <= 0)
                throw new Exception("Id should not be greater than 0.");
            return dataSvc.Book.GetById(id);
        }

        public List<Book> GetAllBooks(bool active)
        {
            //TODO: use active to filter records
            return dataSvc.Book.GetAll().ToList();
        }

        public List<Book> SearchBooksWithTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new Exception("Title should not be empty.");
            }
            try
            {
                var emp = dataSvc.Book.GetAllByCriteria(b => b.Title.ToLower().Contains(title.ToLower())).ToList();
                return emp;
            }
            catch (Exception e)
            {
                //TODO: Logging
                throw e;
            }
        }

        public bool UpdateBook(Book book)
        {
            if (book == null)
            {
                throw new Exception("Book should not be null.");
            }
            try
            {
                dataSvc.Book.Update(book);
                dataSvc.Book.Commit();
                return true;
            }
            catch (Exception e)
            {
                //TODO: Logging
                throw e;
            }
        }

    }
}
