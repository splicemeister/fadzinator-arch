﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Fadz.Interface
{
    public interface IRepository<T> where T : class
    {
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);

        T GetById(object id);
        T GetByCriteria(Func<T, bool> criteria);
        IQueryable<T> GetAll();
        IQueryable<T> GetAllByCriteria(Func<T, bool> criteria);

        IDbSet<T> Entity { get; }
        int Commit();
    }
}
