﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Fadz.Entity;
namespace Fadz.Interface
{
    public interface IDataService
    {
        IRepository<Book> Book { get; }
        IRepository<Category> Category { get; }
    }
}
