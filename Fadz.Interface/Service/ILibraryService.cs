﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Fadz.Entity;

namespace Fadz.Interface
{
    public interface ILibraryService
    {
        int AddBook(Book book);
        Book GetBookById(int id);
        List<Book> GetAllBooks(bool active);
        List<Book> SearchBooksWithTitle(string title);
        bool UpdateBook(Book book);
    }
}
