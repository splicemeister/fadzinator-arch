﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fadz.Entity
{
    public abstract class BaseEntity
    {
        private int id;
        //TODO:Add other properties such as Active, DateCreated etc.

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
