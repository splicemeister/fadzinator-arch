﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LightInject;
using Fadz.Interface;
using Fadz.Service;
using Fadz.Data;

namespace Fadz.Test
{
    public abstract class BaseTest
    {
        private ServiceContainer container;
        public ServiceContainer Container { get { return container; } }
        public BaseTest()
        {
            container = new ServiceContainer();
            container.Register<FadzDbContext>();
            container.Register<IDataService, DataService>();
            container.Register<ILibraryService, LibraryService>();
        }
    }
}