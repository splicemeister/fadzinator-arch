﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fadz.Data;
using Fadz.Entity;
using Fadz.Interface;


namespace Fadz.Test
{
    [TestClass]
    public class FadzRepositoryTest
    {
        [TestClass]
        public class AserpRepositoryTest
        {
            IRepository<Book> repo;

            [TestInitialize]
            public void Setup()
            {
                FadzDbContext context = new FadzDbContext();
                repo = new FadzRepository<Book>(context);
            }

            [TestMethod]
            public void CanAddBook()
            {
                var book = new Book()
                {
                    Title = "Ang Alamat ng Gubat"
                };
                book = repo.Add(book);
                repo.Commit();
                Assert.AreNotEqual(0, book.Id);
            }

            [TestMethod]
            public void CanGetBook()
            {
                var book = repo.GetById(1);
                Assert.IsNotNull(book);
            }

        }
    }
}
