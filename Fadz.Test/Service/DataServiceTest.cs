﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fadz.Interface;
using Fadz.Service;
using Fadz.Entity;


namespace Fadz.Test.Service
{
    [TestClass]
    public class DataServiceTest : BaseTest
    {

        IDataService svc = null;
        [TestInitialize]
        public void SetUp()
        {
            svc = Container.GetInstance<IDataService>();
        }

        [TestMethod]
        public void SanityTest()
        {
            Assert.IsInstanceOfType(svc, typeof(DataService));
        }


        [TestMethod]
        public void CanAddBook()
        {
            var book = new Book()
            {
                Title = "ABNKKBSNPLAko?!"
            };
            book = svc.Book.Add(book);
            svc.Book.Commit();
            Assert.AreNotEqual(0, book.Id);
        }

        [TestMethod]
        public void CanGetBook()
        {
            var book = svc.Book.GetById(1);
            Assert.IsNotNull(book);
        }
    }
}
