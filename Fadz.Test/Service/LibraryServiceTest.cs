﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fadz.Interface;
using Fadz.Service;
using Fadz.Entity;


namespace Fadz.Test.Service
{
    [TestClass]
    public class LibraryServiceTest : BaseTest
    {

        ILibraryService svc = null;
        [TestInitialize]
        public void SetUp()
        {
            svc = Container.GetInstance<ILibraryService>();
        }

        [TestMethod]
        public void SanityTest()
        {
            Assert.IsInstanceOfType(svc, typeof(LibraryService));
        }


        [TestMethod]
        public void CanAddBook()
        {
            var book = new Book()
            {
                Title = "Bakit Baliktad Magbasa ang mga Pilipino?"
            };
            int bookId = svc.AddBook(book);
            Assert.AreNotEqual(0, bookId);
        }

        [TestMethod]
        public void CanGetBook()
        {
            var book = svc.GetBookById(3);
            Assert.IsNotNull(book);
        }
    }
}
