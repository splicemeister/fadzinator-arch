﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Fadz.Entity;

namespace Fadz.Data
{
    public class FadzDbContext : DbContext
    {

        public FadzDbContext()
            : base("FadzDb")
        {
            Database.CreateIfNotExists();
        }

        public DbSet<Book> Book { get; set; }
        public DbSet<Category> Category { get; set; }

    }
}
